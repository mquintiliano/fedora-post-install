# Ansible playbook to manage post-install configurations for Fedora Workstation. 

The **fedora-post-install** is intended for automate ordinary tasks after a Fedora Workstation fresh install. If you prefer to do a fresh install from time to time, just like I do, this playbook should be useful for you as well. The current focus of this playbook is the default Fedora Workstation desktop, which implies the Gnome Shell, but in case you do prefer any other Desktop Environment or Window Manager available, you should be able to easily customize according to your particular needs. 

## How to use 

1. Clone this repository to your fresh installed workstation.
2. Ensure the bootstrap config script has the appropriate permissions in order to be executed.
3. Run the the bootstrap script to see the how to use and what are the available post-install configuration tasks.
5. Review and adjust the list of packages to be installed and/or removed, and also enable/disable some tasks according to your personal preferences.
4. Wait a few minutes while Ansible takes care of your settings.

## Bootstrap 

The bootstrap config is a small script that ensures Ansible is installed on the workstation and then calls our Ansible playbook passing along any specific set of tagged tasks or 

```
$ ./bootstrap_config --help
Usage: bootstrap_config [-h | -l | TASKS]
  
  OPTIONS
    -h, --help         print this help
    -l, --list         list all configuration tasks

  Running specifics configuration tasks
    ./bootstrap_config repos,virt

  NOTE
    With no arguments, bootstrap_config will execute all configuration tasks.
```
>>>
Note: sudo password will be asked in order to actually run the Ansible configuration tasks. 
>>>

## Overriding Defaults

In case you don't wanna run some of the configuration tasks, you can easily skip them by changing the default value of the variables listed on the `config.yml` file (see below) instead of editing the `defaults/main.yml` file for each role.


```
# config.yml - Overriding Defaults

# Role: repos
# Type: Boolean
# Description: Enable RPM Fusion non-free and Google Chrome repositories. 
# Note: Not everyone wants non-free softwares and/or the chrome browser installed, if that's your case, ensure
        no packages coming from these repos are listed into the role packages at `vars/main.yml`.

add_rpmfusion_nonfree: true
enable_chrome_repo: true

# Role: packages
# Type: Boolean
# Description: Remove some software packages installed by default within the workstation. Check `vars/mail.yml`
               for the list of packages to be removed.

delete_software: true


# Role: tweaks
# Type: Boolean
# Description: Install a few shell extensions and apply specific settings tweak to the Gnome desktop.

enable_tweaks: true
install_gnome_extensions: true

# Role: virt
# Type: Boolean
# Description: Add extra tools along with the virtualization software support to the Fedora Workstation.

virtualization_plus: true
```                         

## Managing Software 

The `packages` role allow us to mnage software packages available on your workstation by adding extra packages not installed by default, and optionally removing a few not frequently used. For the default list of packages to be added or removed check the `vars/main.yml` file, and adjust the `extra_packages` and `remove_packages` variables according to your own preferences. 

