Role Name: Suppl
=========

A role to install supplemental software to the workstation, software not packaged and/or available throught a package repository, like rpms or flatpaks.

Requirements
------------

No requirements at all.

Role Variables
--------------

Available variable are listed below (see `defaults/main.yml`):

Add supplemental software to the workstation,

 - suppl_software

Dependencies
------------

None.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: fedora
      roles:
         - { role: suppl }

License
-------

Apache License 2.0
